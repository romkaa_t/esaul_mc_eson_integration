package ru.gazpromneft.esaul_mc_eson_integration.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class TrustStoresConfig {

    @Value("${service.truststore.path}")
    private  String trustStorePath;
    @Value("${service.truststore.password}")
    private  String trustStorePassword;

    @PostConstruct
    public void prepareEnv(){
        System.setProperty("javax.net.ssl.trustStore", trustStorePath);
        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
        System.setProperty("https.protocols", "TLSv1.2");
    }
}
