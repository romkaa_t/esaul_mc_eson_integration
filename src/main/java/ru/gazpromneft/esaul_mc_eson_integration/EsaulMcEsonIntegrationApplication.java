package ru.gazpromneft.esaul_mc_eson_integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"classpath:spring/camel-context.xml"})
public class EsaulMcEsonIntegrationApplication {

  public static void main(String[] args) {
    SpringApplication.run(EsaulMcEsonIntegrationApplication.class, args);
  }

}
